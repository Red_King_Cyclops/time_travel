multidimensions.clear_dimensions() -- clear all dimensions

local ores={
	["default:stone_with_coal"]=200,
	["default:stone_with_iron"]=400,
	["default:stone_with_copper"]=500,
	["default:stone_with_gold"]=2000,
	["default:stone_with_mese"]=10000,
	["default:stone_with_diamond"]=20000,
	["default:mese"]=40000,
	["default:stone_with_metamese"]=40000,
	["default:gravel"]={chance=3000,chunk=2,}
}

--Dinosaur Age Dimension
multidimensions.register_dimension("dinosaur_age",{

  ground_ores = {
	["default:fern_1"] = 2,
	["default:fern_2"] = 4,
	["default:fern_3"] = 8,
	["default:junglegrass"] = 16,
	["default:grass_5"] = 16,
	["paleotest:Cycad"] = 32,
	["paleotest:Horsetails"] = 32,
	["time_travel:jungle_tree"] = 256,
  },
  stone_ores = table.copy(ores),     	     -- works as above, but in stone
  dirt_ores = {},
  grass_ores = {},
  air_ores = {},
  water_ores = {},
  sand_ores = {},
  
  self = {},		    -- can contain everything, var like dirt="default:dirt" will be remade to dirt=content_id
  
  dim_y = 2000,             -- dimension start (don't change if you don't know what you're doing)
  dim_height =  999,	    -- dimension height
  
  
  dirt_start = 501,           -- when dirt begins to appear (default is 501)
  dirt_depth = 3,	    -- dirt depth
  ground_limit = 530,	    -- ground y limit (ground ends here)
  water_depth = 8,	    -- depth fron ground and down
  enable_water = nil,       -- (nil = true)
  terrain_density = 0.4,    -- or ground density
  flatland = nil,           -- (nil = false)
  teleporter = false,         -- (nil = true) dimension teleporter
  gravity = 1,		    -- (1 = default) dimension gravity
  
  stone = "default:stone",
  dirt = "default:dirt",
  grass = "default:dirt_with_coniferous_litter",
  air = "air",
  water = "default:water_source",
  sand = "default:sand",
  bedrock = "multidimensions:bedrock", -- at dimension edges
  
  map = {
    offset = 0,
    scale = 1,
    spread = {x=100,y=18,z=100},
    seeddiff = 24,
    octaves = 5,
    persist = 0.7,
    lacunarity = 1,
    flags = "absvalue",
   },
   
   --[[
   craft = { -- teleport craft recipe
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
	{"default:wood","default:mese","default:wood",},
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
   },
   ]]

--[[
   on_generate=function(self,data,id,area,x,y,z)
	if y <= self.dirt_start+5 then
		data[id] = self.air
	else
		return
	end
	return data -- to return changes
   end,
   ]]
   
   -- data: active generating area (VoxelArea)
   -- index: data index
   -- self: {dim_start, dim_end, dim_height, ground_limit, heat, humidity, dirt, stone, grass, air, water, sand, bedrock ... and your inputs
    ----area: (VoxelArea:new({MinEd...})
   
   sky = {{r=79, g=165, b=92},"plain",{}}, -- same as:set_sky()
   
   on_enter=function(player) --on enter dimension
		--pos = player:get_pos()
		----pos.y = pos.y - 1
		--if minetest.get_node({pos.x, pos.y-1, pos.z}) ~= "multidimensions:teleporter0" and minetest.get_node(pos) ~= "multidimensions:teleporter0" then
		--	minetest.set_node(pos, {name="multidimensions:teleporter0"})
		--end
   end,
   
   on_leave=function(player) --on leave dimension
   end,
   
})

--Ice Age Dimension
multidimensions.register_dimension("ice_age",{

  ground_ores = {
	["default:snow"] = 100,
	["time_travel:snowy_pine_tree"] = 8000,
  },
  stone_ores = table.copy(ores),     	     -- works as above, but in stone
  dirt_ores = {},
  grass_ores = {},
  air_ores = {},
  water_ores = {},
  sand_ores = {},
  
  self = {},		    -- can contain everything, var like dirt="default:dirt" will be remade to dirt=content_id
  
  dim_y = 3000,             -- dimension start (don't change if you don't know what you're doing)
  dim_height =  999,	    -- dimension height
  
  
  dirt_start = 501,           -- when dirt begins to appear (default is 501)
  dirt_depth = 3,	    -- dirt depth
  ground_limit = 530,	    -- ground y limit (ground ends here)
  water_depth = 8,	    -- depth fron ground and down
  enable_water = nil,       -- (nil = true)
  terrain_density = 0.4,    -- or ground density
  flatland = nil,           -- (nil = false)
  teleporter = false,         -- (nil = true) dimension teleporter
  gravity = 1,		    -- (1 = default) dimension gravity
  
  stone = "default:stone",
  dirt = "default:dirt",
  grass = "default:dirt_with_snow",
  air = "air",
  water = "default:ice",
  sand = "default:gravel",
  bedrock = "multidimensions:bedrock", -- at dimension edges
  
  map = {
    offset = 0,
    scale = 1,
    spread = {x=100,y=18,z=100},
    seeddiff = 24,
    octaves = 5,
    persist = 0.7,
    lacunarity = 1,
    flags = "absvalue",
   },
   
   --[[
   craft = { -- teleport craft recipe
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
	{"default:wood","default:mese","default:wood",},
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
   },
   ]]

--[[
   on_generate=function(self,data,id,area,x,y,z)
	if y <= self.dirt_start+5 then
		data[id] = self.air
	else
		return
	end
	return data -- to return changes
   end,
   ]]
   
   -- data: active generating area (VoxelArea)
   -- index: data index
   -- self: {dim_start, dim_end, dim_height, ground_limit, heat, humidity, dirt, stone, grass, air, water, sand, bedrock ... and your inputs
    ----area: (VoxelArea:new({MinEd...})
   
   --sky = {{r=219, g=168, b=117},"plain",{}}, -- same as:set_sky()
   
   on_enter=function(player) --on enter dimension
   end,
   
   on_leave=function(player) --on leave dimension
   end,
   
})

--Prehistoric Australia Dimension
multidimensions.register_dimension("prehistoric_australia",{

  ground_ores = {
	["default:dry_grass_1"] = 25,
	["default:dry_shrub"] = 30000,
	["time_travel:tree"] = 30000,
  },
  stone_ores = table.copy(ores),     	     -- works as above, but in stone
  dirt_ores = {},
  grass_ores = {
  	["time_travel:australia_red_dirt"] = 100,
  },
  air_ores = {},
  water_ores = {},
  sand_ores = {
	["default:water_source"] = 300,
  },
  
  self = {},		    -- can contain everything, var like dirt="default:dirt" will be remade to dirt=content_id
  
  dim_y = 4000,             -- dimension start (don't change if you don't know what you're doing)
  dim_height =  999,	    -- dimension height
  
  
  dirt_start = 501,           -- when dirt begins to appear (default is 501)
  dirt_depth = 3,	    -- dirt depth
  ground_limit = 530,	    -- ground y limit (ground ends here)
  water_depth = 8,	    -- depth fron ground and down
  enable_water = nil,       -- (nil = true)
  terrain_density = 0.4,    -- or ground density
  flatland = nil,           -- (nil = false)
  teleporter = false,         -- (nil = true) dimension teleporter
  gravity = 1,		    -- (1 = default) dimension gravity
  
  stone = "default:stone",
  dirt = "time_travel:australia_red_stone",
  grass = "time_travel:australia_red_gravel",
  air = "air",
  water = "air",
  sand = "time_travel:australia_red_sand",
  bedrock = "multidimensions:bedrock", -- at dimension edges
  
  map = {
    offset = 0,
    scale = 1,
    spread = {x=100,y=18,z=100},
    seeddiff = 24,
    octaves = 5,
    persist = 0.7,
    lacunarity = 1,
    flags = "absvalue",
   },
   
   --[[
   craft = { -- teleport craft recipe
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
	{"default:wood","default:mese","default:wood",},
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
   },
   ]]

--[[
   on_generate=function(self,data,id,area,x,y,z)
	if y <= self.dirt_start+5 then
		data[id] = self.air
	else
		return
	end
	return data -- to return changes
   end,
   ]]
   
   -- data: active generating area (VoxelArea)
   -- index: data index
   -- self: {dim_start, dim_end, dim_height, ground_limit, heat, humidity, dirt, stone, grass, air, water, sand, bedrock ... and your inputs
    ----area: (VoxelArea:new({MinEd...})
   
   --sky = {{r=219, g=168, b=117},"plain",{}}, -- same as:set_sky()
   
   on_enter=function(player) --on enter dimension
   end,
   
   on_leave=function(player) --on leave dimension
   end,
   
})

--Water Age Dimension

	minetest.register_ore({
		ore_type       = "stratum",
		ore            = "multidimensions:bedrock",
		wherein        = "air",
		--clust_scarcity = 9 * 9 * 9,
		--clust_num_ores = 12,
		--clust_size     = 3,
		y_min          = 5000,
		y_max          = 5000,
		--biomes         = {},
        np_stratum_thickness = {
            offset = 8,
            scale = 4,
            spread = {x = 100, y = 100, z = 100},
            seed = 17,
            octaves = 3,
            persist = 0.7
        },
		stratum_thickness = 1,
	})
	
	minetest.register_ore({
		ore_type       = "stratum",
		ore            = "default:stone",
		wherein        = "air",
		--clust_scarcity = 9 * 9 * 9,
		--clust_num_ores = 12,
		--clust_size     = 3,
		y_min          = 5001,
		y_max          = 5500,
		--biomes         = {},
        np_stratum_thickness = {
            offset = 8,
            scale = 4,
            spread = {x = 100, y = 100, z = 100},
            seed = 17,
            octaves = 3,
            persist = 0.7
        },
		stratum_thickness = 500,
	})

	minetest.register_ore({
		ore_type       = "stratum",
		ore            = "default:sand",
		wherein        = "air",
		--clust_scarcity = 9 * 9 * 9,
		--clust_num_ores = 12,
		--clust_size     = 3,
		y_min          = 5501,
		y_max          = 5501,
		--biomes         = {},
        np_stratum_thickness = {
            offset = 8,
            scale = 4,
            spread = {x = 100, y = 100, z = 100},
            seed = 17,
            octaves = 3,
            persist = 0.7
        },
		stratum_thickness = 1,
	})

	minetest.register_ore({
		ore_type       = "stratum",
		ore            = "default:water_source",
		wherein        = "air",
		--clust_scarcity = 9 * 9 * 9,
		--clust_num_ores = 12,
		--clust_size     = 3,
		y_min          = 5502,
		y_max          = 5511,
		--biomes         = {},
        np_stratum_thickness = {
            offset = 8,
            scale = 4,
            spread = {x = 100, y = 100, z = 100},
            seed = 17,
            octaves = 3,
            persist = 0.7
        },
		stratum_thickness = 10,
	})

--[[
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "time_travel:coral_reef",
		wherein        = "default:water_source",
		clust_scarcity = 7 * 7 * 7,
		clust_num_ores = 5,
		clust_size     = 1,
		y_min          = 5501,
		y_max          = 5502,
	})
]]

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:coral_orange",
		wherein        = "default:water_source",
		clust_scarcity = 7 * 7 * 7,
		clust_num_ores = 5,
		clust_size     = 1,
		y_min          = 5501,
		y_max          = 5502,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:coral_brown",
		wherein        = "default:water_source",
		clust_scarcity = 7 * 7 * 7,
		clust_num_ores = 5,
		clust_size     = 1,
		y_min          = 5501,
		y_max          = 5502,
	})

--LBMs and ABMs

minetest.register_lbm({
	name = "time_travel:tree_lbm",
	run_at_every_load = true,
	nodenames = {"time_travel:jungle_tree", "time_travel:snowy_pine_tree", "time_travel:tree"},
	action = function(pos, node)
		minetest.set_node(pos, {name = "air"})
		local tree=""
		if node.name=="time_travel:jungle_tree" then
			tree=minetest.get_modpath("default") .. "/schematics/jungle_tree.mts"
		elseif node.name=="time_travel:snowy_pine_tree" then
			tree=minetest.get_modpath("default") .. "/schematics/snowy_pine_tree_from_sapling.mts"
		elseif node.name=="time_travel:tree" then
			tree=minetest.get_modpath("default") .. "/schematics/apple_tree.mts"
		end
		minetest.place_schematic({x=pos.x,y=pos.y,z=pos.z}, tree, "random", {}, true)
	end,
})

--[[
minetest.register_lbm({
	name = "time_travel:coral_reef_lbm",
	run_at_every_load = true,
	nodenames = {"time_travel:coral_reef"},
	action = function(pos, node)
		minetest.set_node(pos, {name = "default:water_source"})
		local coral_reef=""
		--if node.name=="time_travel:coral_reef" then
			coral_reef=minetest.get_modpath("default") .. "/schematics/corals.mts"
		--end
		minetest.place_schematic(pos, coral_reef, "random", {}, true)
	end
})
]]

minetest.register_abm({
    nodenames = {"time_travel:australia_red_dirt"},
    neighbors = {"default:water_source", "default:water_flowing"},
    interval = 5.0,
    chance = 1,
    action = function(pos, node, active_object_count,
            active_object_count_wider)
        local pos = {x = pos.x, y = pos.y, z = pos.z}
        minetest.set_node(pos, {name = "default:dirt"})
    end
})
