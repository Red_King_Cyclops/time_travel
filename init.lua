dofile(minetest.get_modpath("time_travel") .. "/nodes.lua")
dofile(minetest.get_modpath("time_travel") .. "/dimensions.lua")
dofile(minetest.get_modpath("time_travel") .. "/spawning.lua")

--[[
List of dimensions:
water_age
dinosaur_age
prehistoric_australia
ice_age
]]
